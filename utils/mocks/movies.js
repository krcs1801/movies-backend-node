const moviesMock = [{
    "id": "02dbecfe-1e9f-4e02-aa0a-5dcb05975087",
    "title": "Beach Red",
    "year": 1996,
    "cover": "http://dummyimage.com/192x218.png/ff4444/ffffff",
    "description": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.",
    "duration": 2050,
    "contentRating": "PG-13",
    "source": "https://pinterest.com/in/ante/vestibulum/ante/ipsum/primis/in.jsp?massa=dis&tempor=parturient&convallis=montes&nulla=nascetur&neque=ridiculus&libero=mus&convallis=vivamus&eget=vestibulum&eleifend=sagittis&luctus=sapien&ultricies=cum&eu=sociis&nibh=natoque&quisque=penatibus&id=et&justo=magnis&sit=dis&amet=parturient&sapien=montes&dignissim=nascetur&vestibulum=ridiculus&vestibulum=mus&ante=etiam&ipsum=vel&primis=augue&in=vestibulum&faucibus=rutrum&orci=rutrum&luctus=neque&et=aenean&ultrices=auctor&posuere=gravida&cubilia=sem&curae=praesent&nulla=id&dapibus=massa&dolor=id&vel=nisl&est=venenatis&donec=lacinia&odio=aenean&justo=sit&sollicitudin=amet&ut=justo&suscipit=morbi&a=ut&feugiat=odio&et=cras&eros=mi&vestibulum=pede&ac=malesuada&est=in&lacinia=imperdiet&nisi=et&venenatis=commodo&tristique=vulputate&fusce=justo&congue=in&diam=blandit&id=ultrices&ornare=enim&imperdiet=lorem&sapien=ipsum&urna=dolor&pretium=sit&nisl=amet&ut=consectetuer&volutpat=adipiscing&sapien=elit&arcu=proin&sed=interdum&augue=mauris&aliquam=non&erat=ligula&volutpat=pellentesque&in=ultrices&congue=phasellus&etiam=id&justo=sapien&etiam=in&pretium=sapien&iaculis=iaculis&justo=congue&in=vivamus&hac=metus&habitasse=arcu&platea=adipiscing&dictumst=molestie&etiam=hendrerit&faucibus=at&cursus=vulputate&urna=vitae&ut=nisl&tellus=aenean&nulla=lectus&ut=pellentesque",
    "tags": ["Action|Adventure|Sci-Fi", "Comedy", "Action|Drama|Thriller", "Drama|Romance", "Adventure|Drama|Thriller"]
},
    {
        "id": "96783d29-0a1b-47ce-8814-03aeb2d5f8e4",
        "title": "George Carlin: Life Is Worth Losing",
        "year": 2012,
        "cover": "http://dummyimage.com/175x202.jpg/dddddd/000000",
        "description": "Fusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.",
        "duration": 2038,
        "contentRating": "G",
        "source": "https://bbb.org/semper.aspx?consequat=nascetur&ut=ridiculus&nulla=mus&sed=etiam&accumsan=vel&felis=augue&ut=vestibulum&at=rutrum&dolor=rutrum&quis=neque&odio=aenean&consequat=auctor&varius=gravida&integer=sem&ac=praesent&leo=id&pellentesque=massa&ultrices=id&mattis=nisl&odio=venenatis&donec=lacinia&vitae=aenean&nisi=sit&nam=amet&ultrices=justo&libero=morbi&non=ut&mattis=odio&pulvinar=cras&nulla=mi&pede=pede&ullamcorper=malesuada&augue=in&a=imperdiet&suscipit=et&nulla=commodo&elit=vulputate&ac=justo&nulla=in&sed=blandit&vel=ultrices&enim=enim&sit=lorem&amet=ipsum&nunc=dolor&viverra=sit&dapibus=amet&nulla=consectetuer&suscipit=adipiscing&ligula=elit&in=proin&lacus=interdum",
        "tags": ["Animation|Comedy", "Drama", "Comedy"]
    },
    {
        "id": "9100c098-eeff-4369-aa0f-fa8f44d5ad38",
        "title": "Iron Giant, The",
        "year": 2001,
        "cover": "http://dummyimage.com/148x140.jpg/cc0000/ffffff",
        "description": "Phasellus in felis. Donec semper sapien a libero. Nam dui.\n\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.",
        "duration": 1909,
        "contentRating": "NC-17",
        "source": "https://ed.gov/posuere.html?non=vulputate&ligula=elementum&pellentesque=nullam&ultrices=varius&phasellus=nulla&id=facilisi&sapien=cras&in=non&sapien=velit&iaculis=nec&congue=nisi&vivamus=vulputate&metus=nonummy&arcu=maecenas&adipiscing=tincidunt&molestie=lacus&hendrerit=at&at=velit&vulputate=vivamus&vitae=vel&nisl=nulla&aenean=eget&lectus=eros&pellentesque=elementum&eget=pellentesque&nunc=quisque&donec=porta&quis=volutpat&orci=erat&eget=quisque&orci=erat&vehicula=eros&condimentum=viverra&curabitur=eget&in=congue&libero=eget&ut=semper&massa=rutrum",
        "tags": ["Drama", "Comedy|Drama", "Action|Adventure|Sci-Fi|War", "Comedy"]
    },
    {
        "id": "28d42a81-93a6-4143-a951-f1ff1e046c8c",
        "title": "La Luna",
        "year": 2012,
        "cover": "http://dummyimage.com/112x181.png/ff4444/ffffff",
        "description": "Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\n\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",
        "duration": 1919,
        "contentRating": "R",
        "source": "http://dailymotion.com/vel/enim/sit/amet/nunc.jsp?a=tempus&libero=vivamus",
        "tags": ["Crime|Drama|Mystery|Romance|Thriller", "Drama|Romance", "Crime|Drama|Thriller", "Sci-Fi", "Drama"]
    },
    {
        "id": "735be2d8-3981-4533-bb24-cfae795bb40c",
        "title": "Satan's Tango (Sátántangó)",
        "year": 1999,
        "cover": "http://dummyimage.com/237x241.jpg/cc0000/ffffff",
        "description": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
        "duration": 1973,
        "contentRating": "G",
        "source": "https://walmart.com/scelerisque/mauris.jpg?a=duis&odio=aliquam&in=convallis&hac=nunc&habitasse=proin&platea=at&dictumst=turpis&maecenas=a&ut=pede&massa=posuere&quis=nonummy&augue=integer&luctus=non&tincidunt=velit&nulla=donec&mollis=diam&molestie=neque&lorem=vestibulum&quisque=eget&ut=vulputate&erat=ut&curabitur=ultrices&gravida=vel&nisi=augue&at=vestibulum&nibh=ante&in=ipsum&hac=primis&habitasse=in&platea=faucibus&dictumst=orci&aliquam=luctus&augue=et&quam=ultrices&sollicitudin=posuere&vitae=cubilia&consectetuer=curae&eget=donec&rutrum=pharetra&at=magna&lorem=vestibulum&integer=aliquet&tincidunt=ultrices&ante=erat&vel=tortor&ipsum=sollicitudin&praesent=mi&blandit=sit&lacinia=amet&erat=lobortis&vestibulum=sapien&sed=sapien&magna=non&at=mi&nunc=integer&commodo=ac&placerat=neque&praesent=duis&blandit=bibendum&nam=morbi&nulla=non&integer=quam&pede=nec&justo=dui&lacinia=luctus&eget=rutrum&tincidunt=nulla&eget=tellus&tempus=in&vel=sagittis&pede=dui&morbi=vel&porttitor=nisl&lorem=duis&id=ac&ligula=nibh&suspendisse=fusce&ornare=lacus&consequat=purus&lectus=aliquet&in=at&est=feugiat&risus=non&auctor=pretium&sed=quis&tristique=lectus",
        "tags": ["Action|Animation|Fantasy", "Documentary", "(no genres listed)", "Comedy|Drama|Fantasy|Mystery|Romance", "Documentary"]
    },
    {
        "id": "00cd7256-a2e7-49e2-a971-ee6de72fef7f",
        "title": "Sweet Sweetback's Baadasssss Song",
        "year": 2010,
        "cover": "http://dummyimage.com/112x170.png/ff4444/ffffff",
        "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\n\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.",
        "duration": 2045,
        "contentRating": "NC-17",
        "source": "http://cdbaby.com/suspendisse/potenti/cras/in.html?pellentesque=massa&eget=donec&nunc=dapibus&donec=duis&quis=at&orci=velit&eget=eu&orci=est&vehicula=congue&condimentum=elementum&curabitur=in&in=hac&libero=habitasse&ut=platea&massa=dictumst&volutpat=morbi&convallis=vestibulum&morbi=velit&odio=id&odio=pretium&elementum=iaculis&eu=diam&interdum=erat&eu=fermentum&tincidunt=justo&in=nec&leo=condimentum&maecenas=neque&pulvinar=sapien&lobortis=placerat&est=ante&phasellus=nulla&sit=justo&amet=aliquam&erat=quis&nulla=turpis&tempus=eget&vivamus=elit&in=sodales&felis=scelerisque&eu=mauris&sapien=sit&cursus=amet&vestibulum=eros&proin=suspendisse&eu=accumsan&mi=tortor&nulla=quis&ac=turpis&enim=sed&in=ante&tempor=vivamus&turpis=tortor&nec=duis&euismod=mattis&scelerisque=egestas&quam=metus&turpis=aenean&adipiscing=fermentum&lorem=donec&vitae=ut&mattis=mauris&nibh=eget&ligula=massa&nec=tempor&sem=convallis&duis=nulla&aliquam=neque&convallis=libero&nunc=convallis&proin=eget&at=eleifend&turpis=luctus&a=ultricies&pede=eu&posuere=nibh&nonummy=quisque&integer=id&non=justo&velit=sit&donec=amet&diam=sapien&neque=dignissim&vestibulum=vestibulum&eget=vestibulum&vulputate=ante&ut=ipsum&ultrices=primis&vel=in",
        "tags": ["Documentary", "Comedy"]
    },
    {
        "id": "e87bb002-0448-4453-8ee3-3468a6bf60cd",
        "title": "Demon Wind",
        "year": 2009,
        "cover": "http://dummyimage.com/151x128.jpg/5fa2dd/ffffff",
        "description": "Phasellus in felis. Donec semper sapien a libero. Nam dui.\n\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.",
        "duration": 2046,
        "contentRating": "PG",
        "source": "http://furl.net/vestibulum/ac.xml?lacus=velit&at=vivamus&turpis=vel&donec=nulla&posuere=eget&metus=eros&vitae=elementum&ipsum=pellentesque&aliquam=quisque&non=porta&mauris=volutpat&morbi=erat&non=quisque&lectus=erat&aliquam=eros&sit=viverra&amet=eget&diam=congue&in=eget&magna=semper&bibendum=rutrum&imperdiet=nulla&nullam=nunc&orci=purus&pede=phasellus&venenatis=in&non=felis&sodales=donec&sed=semper&tincidunt=sapien&eu=a&felis=libero&fusce=nam&posuere=dui&felis=proin&sed=leo&lacus=odio&morbi=porttitor&sem=id&mauris=consequat&laoreet=in&ut=consequat&rhoncus=ut&aliquet=nulla&pulvinar=sed&sed=accumsan&nisl=felis&nunc=ut&rhoncus=at&dui=dolor&vel=quis&sem=odio&sed=consequat&sagittis=varius&nam=integer&congue=ac&risus=leo&semper=pellentesque&porta=ultrices&volutpat=mattis&quam=odio&pede=donec&lobortis=vitae&ligula=nisi&sit=nam&amet=ultrices&eleifend=libero&pede=non&libero=mattis&quis=pulvinar&orci=nulla&nullam=pede&molestie=ullamcorper&nibh=augue&in=a&lectus=suscipit&pellentesque=nulla&at=elit&nulla=ac&suspendisse=nulla&potenti=sed&cras=vel&in=enim&purus=sit&eu=amet&magna=nunc",
        "tags": ["Documentary", "Comedy|Romance", "Mystery|Thriller", "Drama|Mystery"]
    },
    {
        "id": "7ae7b38c-36ae-40ca-b0d4-1a370e2dd75c",
        "title": "Soul Man",
        "year": 1992,
        "cover": "http://dummyimage.com/173x100.jpg/ff4444/ffffff",
        "description": "Sed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
        "duration": 2011,
        "contentRating": "G",
        "source": "http://sphinn.com/nulla.xml?nibh=dui&in=vel&quis=nisl&justo=duis&maecenas=ac&rhoncus=nibh&aliquam=fusce&lacus=lacus&morbi=purus&quis=aliquet&tortor=at&id=feugiat",
        "tags": ["Action|Horror|Thriller", "Children|Comedy|Sci-Fi", "Drama|Romance"]
    },
    {
        "id": "6d0f93af-c795-459c-8270-278b1905ec9f",
        "title": "Inkheart",
        "year": 2012,
        "cover": "http://dummyimage.com/116x208.bmp/cc0000/ffffff",
        "description": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
        "duration": 1917,
        "contentRating": "G",
        "source": "https://wsj.com/neque.jsp?nam=donec&congue=quis&risus=orci&semper=eget&porta=orci&volutpat=vehicula&quam=condimentum&pede=curabitur&lobortis=in&ligula=libero&sit=ut&amet=massa&eleifend=volutpat&pede=convallis&libero=morbi&quis=odio&orci=odio&nullam=elementum&molestie=eu&nibh=interdum&in=eu&lectus=tincidunt&pellentesque=in&at=leo&nulla=maecenas&suspendisse=pulvinar&potenti=lobortis&cras=est&in=phasellus&purus=sit&eu=amet&magna=erat&vulputate=nulla&luctus=tempus&cum=vivamus&sociis=in&natoque=felis&penatibus=eu&et=sapien&magnis=cursus&dis=vestibulum&parturient=proin&montes=eu&nascetur=mi&ridiculus=nulla&mus=ac&vivamus=enim&vestibulum=in&sagittis=tempor&sapien=turpis&cum=nec&sociis=euismod&natoque=scelerisque&penatibus=quam&et=turpis&magnis=adipiscing&dis=lorem&parturient=vitae&montes=mattis",
        "tags": ["Drama"]
    },
    {
        "id": "a5258c85-2806-44ea-bd49-50e674cbc2fb",
        "title": "Time to Live, a Time to Die, A (Tong nien wang shi)",
        "year": 2010,
        "cover": "http://dummyimage.com/105x209.jpg/cc0000/ffffff",
        "description": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.\n\nDuis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.",
        "duration": 2013,
        "contentRating": "PG-13",
        "source": "http://list-manage.com/sit/amet/eros/suspendisse/accumsan/tortor/quis.png?quis=augue&turpis=quam&eget=sollicitudin&elit=vitae&sodales=consectetuer&scelerisque=eget&mauris=rutrum&sit=at",
        "tags": ["Action|Drama|Romance", "Action|Comedy|Crime", "Action"]
    }];
module.exports = {
    moviesMock
}
