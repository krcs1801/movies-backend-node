const {confi} = require('../../config');


let withErrorStack = (error, stack) => {
    if (conig.dev) {
        return {error, stack};
    }
};
let logErrors = (err, req, res, next) => {
    console.log(err);
    next(err);
};
let errorHandler = (err, req, res, next) => {
    res.status(err.status || 500);
    res.json(withErrorStack(err.message, err.stack));
};

module.exports = {
    logErrors,
    errorHandler
};
